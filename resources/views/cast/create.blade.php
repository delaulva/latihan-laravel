@extends('layout.master')
@section('title')
Halaman Tambah Cast
@endsection
@section('content')

<form method="POST" action="/cast">
    @csrf
  <div class="form-group">
    <label>nama</label>
    <input type="text" name="nama" class="form-control">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>umur</label>
    <input type="number" name="umur" class="form-control">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>bio</label>
    <textarea name="bio" cols="30" rows="10" class="form-control"></textarea>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection