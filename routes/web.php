<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome'); 
Route::get('/data-tables', 'IndexController@table');

//Route::get('/master', function(){
   // return view('layout.master');
//});

//CURD Cast
//create
Route::get('/cast/create', 'CastController@create'); //mengarah ke form tambah data
Route::post('/cast', 'CastController@store'); //menyimpan data form ke database table cast

//read
Route::get('/cast', 'CastController@index'); //ambil data ke data base di tampilkan  di blade
Route::get('/cast/{cast_id}', 'CastController@show'); //route detail cast

//Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //route untuk mengarah ke form edit
Route::put('/cast/{cast_id}', 'CastController@update'); //

//delete
Route::delete('/cast/{cast_id}', 'CastController@destroy');



