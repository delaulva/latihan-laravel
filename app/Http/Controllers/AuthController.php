<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('page.form');
    }
    public function welcome(Request $request)
    {
        // dd($request->all());
        $namadepan = $request['first_name'];
        $namabelakang = $request['last_name'];

        return view('page.welcome', compact("namadepan", "namabelakang"));
    }
    
}
