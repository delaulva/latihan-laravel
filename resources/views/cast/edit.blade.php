@extends('layout.master')
@section('title')
Halaman Edit Cast
@endsection
@section('content')

<form method="POST" action="/cast/{{$cast->id}}">
    @csrf
    @method('put')
  <div class="form-group">
    <label>nama</label>
    <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>umur</label>
    <input type="number" name="umur" value="{{$cast->umur}}" class="form-control">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>bio</label>
    <textarea name="bio" cols="30" rows="10" class="form-control">{{$cast->bio}}</textarea>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection